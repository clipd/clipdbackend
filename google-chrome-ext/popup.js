// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

document.addEventListener('DOMContentLoaded', function() {

    console.log('DOMContentLoaded event fired. Checking linked account');

    chrome.storage.sync.get('token', function(result) {
        var token = result.token;

        if (!token) {
            chrome.storage.sync.get('email', function (result) {
                var email = result.email;
                if (!email) {
                    console.log("Email have't stored in chrome storage");
                    renderFormInInitState();
                    return;
                }

                console.log('Email saved. Waiting for input link code');
                $('#emailFormDiv').hide();
                $('#linkCodeFormDiv').show();
            });
            return;
        }

        console.log("Token have stored in chrome storage: " + token);
        renderFormInLoggedState();
    });

    $('#unlinkButton').on('click', function(event) {
        console.log('Unlinking current account...');

        chrome.storage.sync.get('token', function(result) {
            var token = result.token;

            if (!token) {
                console.log('Account already unlinked');
                renderFormInInitState();
                return;
            }

            chrome.storage.sync.get('uuid', function(result) {
                var uuid = result.uuid;

                if (!uuid) {
                    console.log('Uuid not found in storage');
                    renderFormInInitState();
                    return;
                }

                var unlinkRequest = JSON.stringify({
                    token: token,
                    uuid: uuid
                });

                $.ajax({
                    type: 'POST',
                    url: 'http://clipd.it:8080/exchange-buffer-web/session/delete',
                    contentType: "application/json",
                    data: unlinkRequest,
                    dataType: 'json',
                    success: function (response) {
                        console.log('Server 200: ' + JSON.stringify(response));
                        if (!response) {
                            console.error('Empty response from server');
                            renderStatus('Server error');
                            return;
                        }

                        // renderStatus(response.responseMessage);

                        if (response.responseCode == 'OK') {
                            console.log('Account successfully unlinked');
                            renderFormInInitState();
                        } else if (response.responseCode == 'FAIL_NOT_AUTHORIZED') {
                            console.log('Not authorized');
                            renderFormInInitState();
                        } else {
                            renderStatus(response.responseMessage);
                        }
                    },
                    error: function (data) {
                        renderStatus('Server error: ' + data);
                    }
                });
            });
        });
    });
});

$(function() {
    // Get the form.
    var emailFormDiv = $('#emailFormDiv');
    var emailForm = $('#emailForm');
    var emailField = $('#email');
    var linkCodeForm = $('#linkCodeForm');
    var linkCodeField = $('#linkCode');
    // var bufferFormDiv = $('#bufferFormDiv');
    // var bufferForm = $('#bufferForm');
    // var bufferField = $('#buffer');

    // Set up an event listener for the contact form.
    emailForm.submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        var email = emailField.val();

        var createLinkRequest = JSON.stringify({
            email: emailField.val()
        });

        renderStatus('Submitting form...');
        console.log('Submitting form ' + createLinkRequest);

        // Submit the form using AJAX.
        $.ajax({
            type: 'POST',
            url: $(emailForm).attr('action'), // /user/link/create
            contentType: "application/json",
            data: createLinkRequest,
            dataType: 'json',
            success: function (response) {
                console.log('Server 200');
                if (!response) {
                    console.error('Empty response from server');
                    renderStatus('Server error');
                    return;
                }

                console.log(JSON.stringify(response));
                renderStatus(response.responseMessage);

                if (response.responseCode == 'OK') {
                    console.log('Link code created and sent successfully');

                    chrome.storage.sync.set({'email': email}, function () {
                        console.log('Email saved in chrome.storage.sync');
                    });

                    // hide email form
                    $('#emailFormDiv').hide();
                    $('#linkCodeFormDiv').show();
                } else {
                    renderStatus(response.responseMessage);
                    emailField.val('');
                }
            },
            error: function (data) {
                renderStatus('Server error: ' + data);
            }
        });
    });

    linkCodeForm.submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        var linkRequest = JSON.stringify({
            deviceId: "chrome...",
            deviceType: "WEB",
            brand: "Google Chrome",
            device: "<>",
            ip: "192.168.1.0",
            locale: "en_US",
            linkCode: linkCodeField.val()
        });

        renderStatus('Submitting form...');
        console.log('Submitting link code form ' + linkRequest);

        // Submit the form using AJAX.
        $.ajax({
            type: 'POST',
            url: linkCodeForm.attr('action'), // /user/link
            contentType: "application/json",
            data: linkRequest,
            dataType: 'json',
            success: function (response) {
                console.log('Server 200: ' + JSON.stringify(response));
                if (!response) {
                    console.error('Empty response from server');
                    renderStatus('Server error');
                    return;
                }

                // renderStatus(response.responseMessage);
                renderStatus(emailField.val());

                if (response.responseCode == 'OK') {
                    console.log('Extension linked successfully');
                    chrome.storage.sync.set({'token': response.token}, function () {
                        console.log('Token saved in chrome storage sync');
                    });
                    chrome.storage.sync.set({'uuid': response.uuid}, function() {
                        console.log('Uuid saved in chrome storage sync');
                    });

                    renderFormInLoggedState();

                    registerGcmAndAssign(response.token);

                    // $('#bufferFormDiv').show();
                } else {
                    renderStatus(response.responseMessage);
                    linkCodeField.val('');
                }
            },
            error: function (data) {
                renderStatus('Server error: ' + data);
            }
        });
    });

    $('#resendLinkCodeBtn').on("click", function (event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        console.log('Resetting...');

        renderFormInInitState();
    });

});

function tokenAssign(token, notifyToken) {
    $.ajax({
        type: 'POST',
        url: 'http://clipd.it:8080/exchange-buffer-web/notification/token/assign',
        contentType: "application/json",
        data: JSON.stringify({
            token: token,
            notifyToken: notifyToken
        }),
        dataType: 'json',
        success: function (response) {
            console.log('Server 200');
            if (!response) {
                console.error('Empty response from server');
                // renderStatus('Server error');
                return;
            }

            console.log('Response: ' + JSON.stringify(response));

            // renderStatus(response.responseMessage);

            if (response.responseCode == 'OK') {
                console.log('Firebase token assigned successfully');
                // renderStatus('Notification subsystem done')
            } else {
                // renderStatus(response.responseMessage);
            }
        },
        error: function (data) {
            console.log('Failed to assign token: ' + data);
            // renderStatus('Server error');
        }
    });
}

function renderStatus(statusText) {
    $('#status').text(statusText);
}

function renderFormInLoggedState() {
    $('#emailFormDiv').hide();
    $('#linkCodeFormDiv').hide();
    $('#unlinkButtonDiv').show();
    $('#historyTableDiv').show();
    chrome.storage.sync.get('email', function(result) {
        renderStatus('Hi, ' + result.email);
    });

    chrome.storage.sync.get('token', function(result) {
        var token = result.token;
        if (!token) {
            console.log('Token not found in storage');
            return;
        }
        console.log('Requesting history...');
        $.ajax({
            type: 'POST',
            url: 'http://clipd.it:8080/exchange-buffer-web/notification/history',
            contentType: "application/json",
            data: JSON.stringify({token: token}),
            dataType: 'json',
            success: function (response) {
                console.log('Server 200');
                if (!response) {
                    console.error('Empty response from server');
                    // renderStatus('Server error');
                    return;
                }

                console.log('Response: ' + JSON.stringify(response));

                // renderStatus(response.responseMessage);

                if (response.responseCode == 'OK') {
                    console.log('History fetched successfully');
                    fillHistoryTable(response.messages);
                } else {
                    console.log('Failed to fetch history: ' + response.responseMessage);
                }
            },
            error: function (data) {
                console.log('Failed to fetch history: ' + data);
            }
        });
    });
}

function fillHistoryTable(historyList) {
    var content = [], historyTableContentString;
    content.push("<tr><td>time</td><td>brand</td><td>device</td><td>text</td><td></td></tr>");
    $.each(historyList, function (recordIndex, record) {
        historyTableContentString = "<tr>";
        historyTableContentString += "<td>" + getTime(record.lastModified) + "</td>";
        historyTableContentString += "<td>" + record.session.brand + "</td>";
        historyTableContentString += "<td>" + record.session.device + "</td>";
        historyTableContentString += '<td class="copy-text">' + escape(record.text) + "</td>";
        historyTableContentString += '<td><button class="copy-btn" data-text="' + record.uuid + '">Copy</button></td>';
        historyTableContentString += "</tr>";
        content.push(historyTableContentString);
    });
    $('#historyTable').html(content);
    $('.copy-btn').on("click", function(event) {
        console.log('In onclick copy button handler. Event:');
        console.dir(event);
        var btn = event.target;
        console.dir(btn);
        var copyTr = btn.parentElement.parentElement;
        console.dir(copyTr);
        // var copyTextTd = copyTr.find(".copy-text");
        var copyTextTd = $('.copy-text', copyTr);
        console.dir(copyTextTd);
        var copyText = copyTextTd.text();
        console.log('Copying text: ' + copyText);
        copyToBuffer(copyText);
    });
}

function copyToBuffer(text) {
    $('body').append('<textarea id="temp"/>');
    var tempTextarea = $("#temp");
    tempTextarea.text(text).select(), document.execCommand("copy"), tempTextarea.remove()
}

function escape(str) {
    return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;")
}

function getTime(timestamp) {
    // Create a new JavaScript Date object based on the timestamp
    var date = new Date(timestamp);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}

function renderFormInInitState() {
    chrome.storage.sync.clear(function () {
        console.log('Chrome storage sync cleared');
    });
    $('#emailFormDiv').show();
    $('#linkCodeFormDiv').hide();
    $('#historyTableDiv').hide();
    $('#unlinkButtonDiv').hide();
    renderStatus('Link your account:');
}

function registerGcmAndAssign(token) {
    console.log('Checking notify token, stored in chrome storage...');

    chrome.storage.sync.get('notifyToken', function(result) {

        if (result.notifyToken) {
            console.log('Gcm already registered')
            tokenAssign(token, result.notifyToken);
        }

        console.log('Sending request to GCM...');

        // Up to 100 senders are allowed.
        var senderIds = ["585788175855"];
        chrome.gcm.register(senderIds, function(registrationId) {
            if (chrome.runtime.lastError) {
                renderStatus('Failed to register notification token');
                console.log('Token registration error: ' + JSON.stringify(chrome.runtime.lastError));
                return;
            }
            console.log('GCM registration id: ' + registrationId);
            tokenAssign(token, registrationId);

            chrome.storage.sync.set({'notifyToken': registrationId}, function () {
                console.log('Notify token saved in chrome.storage.sync')
            });
        });
    });
}

// On paste file to the page via drag and drop
// document.onpaste = function(event){
//     var items = (event.clipboardData || event.originalEvent.clipboardData).items;
//     console.log(JSON.stringify(items)); // will give you the mime types
//     for (index in items) {
//         var item = items[index];
//         if (item.kind === 'file') {
//             var blob = item.getAsFile();
//             var reader = new FileReader();
//             reader.onload = function(event){
//                 console.log(event.target.result)}; // data url!
//             // reader.readAsDataURL(blob); // - to draw picture on page
//             reader.readAsBinaryString(blob); // - to upload file to server
//         }
//     }
// }
