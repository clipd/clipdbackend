/**
 * Created by toroptsev on 21.08.16.
 */
$(function () {
    var lastReceivedBuffer;
    var lastSentBuffer;

    chrome.gcm.onMessage.addListener(function(message) {
        console.dir(message);

        // PUSH
        if (message.data["gcm.notification.body"]) {
            console.log("New push notification received in background");
            chrome.notifications.create(
                'push-notification',
                {
                    type: 'basic',
                    iconUrl: 'icon.png',
                    title: message.data["gcm.notification.title"],
                    message: message.data["gcm.notification.body"]
                },
                function () {}
            );
            return;
        }

        console.log("New message received in background: " + message.data.body);

        chrome.notifications.create(
            'buffer-notification',
            {
                type: 'basic',
                iconUrl: 'icon.png',
                title: "Exchange buffer",
                message: "New buffer received: " + message.data.body
            },
            function () {
                // alert(message.data.body);
            }
        );

        pasteInBuffer(message.data.body);
    });

    var textArea = $("#text");
    trackSystemClipboard();

    function trackSystemClipboard() {
        setInterval(function () {
            textArea.focus().val("");
            document.execCommand("paste");
            var clipboardValue = textArea.val();
            if (clipboardValue && clipboardValue !== lastSentBuffer && clipboardValue !== lastReceivedBuffer) {
                sendBufferToServer(clipboardValue);
            }
        }, 500);
    }

    function pasteInBuffer(text) {
        lastReceivedBuffer = text;
        $('body').append('<textarea id="temp"/>');
        var e = $("#temp");
        e.text(text).select(), document.execCommand("copy"), e.remove()
        // textArea.text(text).select();
        // document.execCommand("copy");
    }

    function sendBufferToServer(text) {
        lastSentBuffer = text;
        chrome.storage.sync.get('token', function(result) {
            var token = result.token;
            if (!token) {
                console.log("token haven't saved in local chrome storage");
                return;
            }

            var notificationRequest = JSON.stringify({
                token: token,
                message: text
            });

            console.log('Submitting notification request: ' + notificationRequest);

            $.ajax({
                type: 'POST',
                url: 'http://clipd.it:8080/exchange-buffer-web/notification/send',
                contentType: "application/json",
                data: notificationRequest,
                dataType: 'json',
                success: function (response) {
                    console.log('Server 200: ' + JSON.stringify(response));
                    if (!response) {
                        console.error('Empty response from server');
                        return;
                    }

                    console.log(response.responseMessage);

                    if (response.responseCode == 'OK') {
                        console.log('Notification sent successfully');
                    } else if (response.responseCode == 'FAIL_NOT_AUTHORIZED') {
                        console.log('Token not valid. Deleting');
                        chrome.storage.sync.clear(function() {
                            // TODO check chrome.error...
                            console.log('Storage successfully cleared');
                        });
                    } else {
                        console.log(response.responseMessage);
                    }
                },
                error: function (data) {
                    console.log('Failed to send buffer to server:');
                    console.dir(data);
                }
            });
        });
    }
});