package it.clipd.rest;

import it.clipd.facade.NotificationFacade;
import it.clipd.request.BaseRequest;
import it.clipd.request.NotificationRequest;
import it.clipd.request.TokenAssignRequest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by toroptsev on 04.06.16.
 */
@Path("/notification")
public class NotificationResource {

    @EJB
    private NotificationFacade notificationFacade;

    @POST
    @Path("/token/assign")
    @Consumes("application/json")
    @Produces("application/json")
    public Response assignToken(TokenAssignRequest request) {
        try {
            return Response.ok().entity(notificationFacade.assignToken(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/send")
    @Consumes("application/json")
    @Produces("application/json")
    public Response notify(NotificationRequest request) {
        try {
            return Response.ok().entity(notificationFacade.sendNotificationToAllDevices(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/history")
    @Consumes("application/json")
    @Produces("application/json")
    public Response history(BaseRequest request) {
        try {
            return Response.ok().entity(notificationFacade.history(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }
}
