package it.clipd.rest;

import it.clipd.facade.SessionFacade;
import it.clipd.request.BaseRequest;
import it.clipd.request.SessionDeleteRequest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by toroptsev on 26.08.16.
 */
@Path("/session")
public class SessionResource {

    @EJB
    private SessionFacade sessionFacade;

    @POST
    @Path("/list")
    @Consumes("application/json")
    @Produces("application/json")
    public Response list(BaseRequest request) {
        try {
            return Response.ok().entity(sessionFacade.list(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/delete")
    @Consumes("application/json")
    @Produces("application/json")
    public Response delete(SessionDeleteRequest request) {
        try {
            return Response.ok().entity(sessionFacade.delete(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }
}
