package it.clipd.rest.common;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Calendar;

/**
 * Created by toroptsev on 10.05.16.
 */
@Path("/")
public class StatusResource {

    @GET
    @Path("/status")
    @Produces("application/json")
    public Response status() throws Exception {
        try {
            return Response.ok().entity(Calendar.getInstance().getTime()).build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }
}
