package it.clipd.rest;

import it.clipd.facade.UserFacade;
import it.clipd.request.*;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by toroptsev on 28.05.16.
 */
@Path("/user")
public class UserResource {

    @EJB
    private UserFacade userFacade;

    @POST
    @Path("/auth")
    @Consumes("application/json")
    @Produces("application/json")
    public Response auth(RegisterRequest request) {
        try {
            return Response.ok().entity(userFacade.auth(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/link/create")
    @Consumes("application/json")
    @Produces("application/json")
    public Response createLink(RegisterRequest request) {
        try {
            return Response.ok().entity(userFacade.createLink(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/link")
    @Consumes("application/json")
    @Produces("application/json")
    public Response link(LinkRequest request) {
        try {
            return Response.ok().entity(userFacade.link(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }

    @OPTIONS
    @Path("/link")
    public Response linkOptions(@HeaderParam("ORIGIN") String origin) {
        return Response.ok()
                .header("Access-Control-Allow-Origin", origin)
                .header("Access-Control-Allow-Methods", "POST")
                .header("Access-Control-Allow-Headers", "accept, content-type")
                .header("Access-Control-Max-Age", "1728000")
                .build();
    }

    @POST
    @Path("/delete")
    @Consumes("application/json")
    @Produces("application/json")
    public Response delete(DeleteUserRequest request) {
        try {
            return Response.ok().entity(userFacade.delete(request)).build();
        } catch (Throwable throwable) {
            return Response.serverError().build();
        }
    }

}
