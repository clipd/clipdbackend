package it.clipd.facade;

import it.clipd.dao.SessionDAO;
import it.clipd.model.Session;
import it.clipd.request.BaseRequest;
import it.clipd.request.SessionDeleteRequest;
import it.clipd.response.BaseResponse;
import it.clipd.response.ResponseCode;
import it.clipd.response.SessionsResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by toroptsev on 28.08.16.
 */
@Stateless
@LocalBean
public class SessionFacade {
    
    @Inject
    private Logger logger;

    @EJB
    private SessionDAO sessionDAO;
    
    public BaseResponse delete(SessionDeleteRequest request) {
        if (StringUtils.isEmpty(request.getToken())) {
            logger.warn("Empty token");
            return new BaseResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Empty token");
        }
        Session session = sessionDAO.findByToken(request.getToken());
        if (session == null) {
            logger.warn("Session with token " + request.getToken() + " not found");
            return new BaseResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Session not found");
        }
        if (StringUtils.isEmpty(request.getUuid())) {
            logger.warn("Empty uuid");
            return new BaseResponse(ResponseCode.FAIL, "Empty uuid");
        }

        boolean success = sessionDAO.deleteByUuid(request.getUuid());

        return new BaseResponse(ResponseCode.OK, success ? "Session successfully deleted" : "Session to delete not found");
    }

    public SessionsResponse list(BaseRequest request) {
        Session session = sessionDAO.findByToken(request.getToken());
        if (session == null) {
            logger.warn("Session with token " + request.getToken() + " not found");
            return new SessionsResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Session not found");
        }

        List<Session> userSessions = sessionDAO.listByUser(session.getUser());
        userSessions.forEach(Session::clearPrivateInfo);
        return new SessionsResponse(userSessions);
    }
}
