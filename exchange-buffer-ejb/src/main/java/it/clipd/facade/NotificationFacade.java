package it.clipd.facade;

import it.clipd.business.FirebaseCloudMessaginService;
import it.clipd.dao.MessageDAO;
import it.clipd.dao.SessionDAO;
import it.clipd.dao.UserDAO;
import it.clipd.model.Message;
import it.clipd.model.PackageName;
import it.clipd.model.Session;
import it.clipd.model.User;
import it.clipd.request.BaseRequest;
import it.clipd.request.NotificationRequest;
import it.clipd.request.TokenAssignRequest;
import it.clipd.response.BaseResponse;
import it.clipd.response.MessagesResponse;
import it.clipd.response.ResponseCode;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * Created by toroptsev on 01.06.16.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NotificationFacade {

    @Inject
    private Logger logger;

    @EJB
    private UserDAO userDAO;
    @EJB
    private SessionDAO sessionDAO;
    @EJB
    private MessageDAO messageDAO;
    @EJB
    private FirebaseCloudMessaginService messaginService;

    public BaseResponse sendNotificationToAllDevices(NotificationRequest request) {
        if (StringUtils.isBlank(request.getMessage())) {
            logger.warn("Empty message");
            return new BaseResponse(ResponseCode.FAIL, "Message is empty");
        }
        Session session = sessionDAO.findByToken(request.getToken());
        if (session == null) {
            logger.warn("Session not found");
            return new BaseResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Session not found");
        }

        saveMessage(request.getMessage(), session);

        return sendNotificationToAllDevices(session.getUser(), request.getMessage(), null, request.getToken());
    }

    public BaseResponse sendNotificationToAllDevices(User user, String message, String title, String excludeToken) {
        List<Session> sessions = sessionDAO.listWithNotifyTokenByUser(user);
        logger.info(sessions.size() + " sessions found for user " + user.getEmail());

        for (Session receiverSession : sessions) {
            if (receiverSession.getToken().equals(excludeToken)) {
                logger.info("Excluding notification token: " + receiverSession.getNotifyToken());
                continue;
            }

            PackageName pkg = PackageName.getByPackageName(receiverSession.getPackageName());
            if (pkg != null && pkg.isDebug()) {
                messaginService.sendDebugMessage(receiverSession.getNotifyToken(), title, message);
            } else {
                messaginService.sendMessage(receiverSession.getNotifyToken(), title, message);
            }
        }

        return new BaseResponse(ResponseCode.OK, "Successful");
    }

    public BaseResponse assignToken(TokenAssignRequest request) {
        Session session = sessionDAO.findByToken(request.getToken());
        if (session == null) {
            return new BaseResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Session not found");
        }
        if (StringUtils.isBlank(request.getNotifyToken())) {
            return new BaseResponse(ResponseCode.FAIL, "Notify token is empty");
        }

        session.setNotifyToken(request.getNotifyToken());
        // TODO вспомнить, почему обычный апдейт хуже и зачем (если это так) выше сессии присваивается notifyToken?
        sessionDAO.assignNotifyToken(session.getToken(), request.getNotifyToken());

        return new BaseResponse(ResponseCode.OK, "Token assigned successfully");
    }

    public MessagesResponse history(BaseRequest request) {
        Session session = sessionDAO.findByToken(request.getToken());
        if (session == null) {
            return new MessagesResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Session not found");
        }

        List<Message> messages = messageDAO.listByUser(session.getUser());
        messages.forEach(Message::clearPrivateInfo);
        return new MessagesResponse(messages);
    }

    private Message saveMessage(String message, Session session) {
        Message msg = new Message();
        msg.setText(message);
        msg.setSession(session);
        msg.setLastModified(Calendar.getInstance().getTimeInMillis());
        msg.setUuid(UUID.randomUUID().toString().toUpperCase());
        return messageDAO.insertOne(msg);
    }
}
