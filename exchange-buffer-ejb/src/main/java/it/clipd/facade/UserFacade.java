package it.clipd.facade;

import it.clipd.dao.SessionDAO;
import it.clipd.dao.UserDAO;
import it.clipd.model.Session;
import it.clipd.model.User;
import it.clipd.request.DeleteUserRequest;
import it.clipd.request.LinkRequest;
import it.clipd.request.RegisterRequest;
import it.clipd.response.BaseResponse;
import it.clipd.response.RegisterResponse;
import it.clipd.response.ResponseCode;
import it.clipd.util.GoogleVerifier;
import it.clipd.util.LinkCodeGenerator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.UUID;

/**
 * Created by toroptsev on 28.05.16.
 */
@Stateless
@LocalBean
public class UserFacade {

    @Inject
    private Logger logger;
    @EJB
    private UserDAO userDAO;
    @EJB
    private SessionDAO sessionDAO;
//    @EJB
//    private SessionUUIDGenerator sessionUuidGenerator;
    @EJB
    private LinkCodeGenerator linkCodeGenerator;
    @EJB
    private GoogleVerifier googleVerifier;
    @EJB
    private NotificationFacade notificationFacade;

    public RegisterResponse auth(RegisterRequest request) {
        if (StringUtils.isBlank(request.getEmail())) {
            logger.warn("Empty email");
            return new RegisterResponse(ResponseCode.FAIL, "Email is empty");
        }
        if (StringUtils.isBlank(request.getGoogleToken())) {
            logger.warn("Empty googleToken");
            return new RegisterResponse(ResponseCode.FAIL, "Google token is empty");
        }
        if (StringUtils.isBlank(request.getPackageName())) {
            logger.warn("Empty packageName");
            return new RegisterResponse(ResponseCode.FAIL, "Package name is empty");
        }
        if (request.getDeviceType() == null) {
            logger.warn("Empty deviceType");
            return new RegisterResponse(ResponseCode.FAIL, "Device type is empty");
        }
        User user;
        try {
            User verifiedUser = googleVerifier.verify(request.getGoogleToken(), request.getEmail(), request.getPackageName());
            if (verifiedUser == null) {
                return new RegisterResponse(ResponseCode.FAIL, "Invalid google idToken");
            }
            user = userDAO.findByEmail(request.getEmail());
            if (user == null) {
                logger.info("User " + verifiedUser.getEmail() + " not found. Creating new one");
                user = createNewUser(verifiedUser);
            } else {
                user = updateUser(user, verifiedUser);
            }
        } catch (Exception e) {
            logger.error("Failed to verify google idToken");
            return new RegisterResponse(ResponseCode.FAIL, "Failed to verify google idToken");
        }

        Session session = createNewSession(user, request);
        logger.info("New session with token " + session.getToken() + " created for user " + user.getEmail());

        RegisterResponse response = new RegisterResponse(ResponseCode.OK, "User successfully registered");
        response.setToken(session.getToken());
        response.setUuid(session.getUuid());

        return response;
    }

    public BaseResponse createLink(RegisterRequest request) {
        if (StringUtils.isEmpty(request.getEmail())) {
            logger.warn("Empty email");
            return new RegisterResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Empty email");
        }

        User user = userDAO.findByEmail(request.getEmail());
        if (user == null) {
            logger.warn("User with email " + request.getEmail() + " not found");
            return new RegisterResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "User not found");
        }

        if (!sessionDAO.hasAnySession(user)) {
            logger.warn("No sessions for user " + user.getEmail());
            return new RegisterResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "There is no devices registered");
        }

        String linkCode = linkCodeGenerator.generate();
        logger.info("New link code genereated: " + linkCode);
        user.setLinkCode(linkCode);
        user.setLastModified(Calendar.getInstance().getTimeInMillis());
        userDAO.updateOne(user);

        notificationFacade.sendNotificationToAllDevices(user, "Your link code: " + linkCode, "Info", null); // push notification
//        notificationFacade.sendNotificationToAllDevices(user, "Your link code: " + linkCode, null, null); // payload

        return new BaseResponse(ResponseCode.OK, "Link created successfully");
    }

    public RegisterResponse link(LinkRequest request) {
        if (StringUtils.isBlank(request.getLinkCode())) {
            logger.warn("Empty linkCode");
            return new RegisterResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "Link is empty");
        }
        if (request.getDeviceType() == null) {
            logger.warn("Empty deviceType");
            return new RegisterResponse(ResponseCode.FAIL, "Device type is empty");
        }

        User user = userDAO.findByLinkCode(request.getLinkCode());
        if (user == null) {
            logger.warn("User with link " + request.getLinkCode() + " not found");
            return new RegisterResponse(ResponseCode.FAIL_NOT_AUTHORIZED, "User not found");
        }

        userDAO.deleteLinkCode(user);

        Session newSession = createNewSession(user, request);

        RegisterResponse response = new RegisterResponse(ResponseCode.OK, "User successfully linked");
        response.setToken(newSession.getToken());
        response.setUuid(newSession.getUuid());

        return response;
    }

    private User updateUser(User user, User verifiedUser) {
        user.setPicture(verifiedUser.getPicture());
        user.setName(verifiedUser.getName());
        user.setFamilyName(verifiedUser.getFamilyName());
        user.setGivenName(verifiedUser.getGivenName());
        user.setLocale(verifiedUser.getLocale());
        user.setLastModified(Calendar.getInstance().getTimeInMillis());
        return userDAO.updateOne(user);
    }

    public BaseResponse delete(DeleteUserRequest request) {
        User user = userDAO.findByToken(request.getToken());
        if (user == null) {
            logger.warn("User with token " + request.getToken() + " not found");
            return new BaseResponse(ResponseCode.FAIL, "User not found");
        }
        if (!user.getEmail().equalsIgnoreCase(request.getEmail())) {
            logger.warn("User with token " + request.getToken() + " found, but emails are not the same. " +
                    "Expected: " + user.getEmail() + ". Actually: " + request.getEmail());
            return new BaseResponse(ResponseCode.FAIL, "Invalid email");
        }
        logger.info("User " + user.getEmail() + " will be removed");

        long deleted = userDAO.deleteByEmail(request.getEmail());
        logger.info(deleted + " records deleted");

        return new BaseResponse(ResponseCode.OK, "User deleted successfully");
    }

    private User createNewUser(User verifiedUser) {
        verifiedUser.setLastModified(Calendar.getInstance().getTimeInMillis());
        return userDAO.insertOne(verifiedUser);
    }

    private Session createNewSession(User user, RegisterRequest request) {
        Session session = new Session();
        session.setUser(user);
//        session.setToken(sessionUuidGenerator.generateToken());
        session.setToken(UUID.randomUUID().toString());
//        session.setUuid(sessionUuidGenerator.generateUUID());
        session.setUuid(UUID.randomUUID().toString().toUpperCase());
        session.setNotifyToken(request.getNotifyToken());
        session.setBoard(request.getBoard());
        session.setBrand(request.getBrand());
        session.setDeviceId(request.getDeviceId());
        session.setDevice(request.getDevice());
        session.setDeviceType(request.getDeviceType().name());
        session.setIp(request.getIp());
        session.setVersionSdk(request.getVersionSdk());
        session.setVersionRelease(request.getVersionRelease());
        session.setVersionIncremental(request.getVersionIncremental());
        session.setVersionCode(request.getVersionCode());
        session.setVersionName(request.getVersionName());
        if (StringUtils.isNotBlank(request.getLocale())) {
            session.setLocale(request.getLocale());
        } else {
            session.setLocale(user.getLocale());
        }
        session.setPackageName(request.getPackageName());
        session.setLastModified(Calendar.getInstance().getTimeInMillis());
        return sessionDAO.insertOne(session);
    }


}
