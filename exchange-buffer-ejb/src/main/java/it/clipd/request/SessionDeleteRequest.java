package it.clipd.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by toroptsev on 24.08.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDeleteRequest extends BaseRequest {

    @JsonProperty
    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
