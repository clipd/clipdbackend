package it.clipd.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by toroptsev on 30.05.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteUserRequest extends BaseRequest {

    @JsonProperty
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
