package it.clipd.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by toroptsev on 30.05.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseRequest implements Serializable {

    @JsonProperty
    protected String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
