package it.clipd.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by toroptsev on 01.06.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationRequest extends BaseRequest {

    @JsonProperty
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
