package it.clipd.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by toroptsev on 04.06.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenAssignRequest extends BaseRequest {

    @JsonProperty
    private String notifyToken;

    public String getNotifyToken() {
        return notifyToken;
    }

    public void setNotifyToken(String notifyToken) {
        this.notifyToken = notifyToken;
    }
}
