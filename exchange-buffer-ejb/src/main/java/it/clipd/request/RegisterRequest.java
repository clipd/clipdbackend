package it.clipd.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.clipd.model.DeviceType;

import java.io.Serializable;

/**
 * Created by toroptsev on 28.05.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequest implements Serializable {

    @JsonProperty
    private String email;
    @JsonProperty
    private String googleToken;
    @JsonProperty
    private String notifyToken;
    @JsonProperty
    private String versionRelease;
    @JsonProperty
    private String versionIncremental;
    @JsonProperty
    private String versionSdk;
    @JsonProperty
    private String board;
    @JsonProperty
    private String brand;
    @JsonProperty
    private String device;
    @JsonProperty
    private DeviceType deviceType;
    @JsonProperty
    private String deviceId;
    @JsonProperty
    private String versionCode;
    @JsonProperty
    private String versionName;
    @JsonProperty
    private String ip;
    @JsonProperty
    private String locale;
    @JsonProperty
    private String packageName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGoogleToken() {
        return googleToken;
    }

    public void setGoogleToken(String googleToken) {
        this.googleToken = googleToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getVersionSdk() {
        return versionSdk;
    }

    public void setVersionSdk(String versionSdk) {
        this.versionSdk = versionSdk;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getNotifyToken() {
        return notifyToken;
    }

    public void setNotifyToken(String notifyToken) {
        this.notifyToken = notifyToken;
    }

    public String getVersionRelease() {
        return versionRelease;
    }

    public void setVersionRelease(String versionRelease) {
        this.versionRelease = versionRelease;
    }

    public String getVersionIncremental() {
        return versionIncremental;
    }

    public void setVersionIncremental(String versionIncremental) {
        this.versionIncremental = versionIncremental;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
