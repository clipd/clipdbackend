package it.clipd.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by toroptsev on 25.07.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkRequest extends RegisterRequest {

    private String linkCode;

    public String getLinkCode() {
        return linkCode;
    }

    public void setLinkCode(String linkCode) {
        this.linkCode = linkCode;
    }
}
