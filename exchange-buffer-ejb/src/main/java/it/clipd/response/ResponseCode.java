package it.clipd.response;

/**
 * Created by toroptsev on 30.05.16.
 */
public enum ResponseCode {

    OK,
    FAIL,
    FAIL_NOT_AUTHORIZED
}
