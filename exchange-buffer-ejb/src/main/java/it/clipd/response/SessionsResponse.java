package it.clipd.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.clipd.model.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toroptsev on 28.08.16.
 */
public class SessionsResponse extends BaseResponse {

    @JsonProperty
    private List<Session> sessions = new ArrayList<>();

    public SessionsResponse() {}

    public SessionsResponse(ResponseCode responseCode, String responseMessage) {
        super(responseCode, responseMessage);
    }

    public SessionsResponse(List<Session> sessions) {
        this.sessions = sessions;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }
}
