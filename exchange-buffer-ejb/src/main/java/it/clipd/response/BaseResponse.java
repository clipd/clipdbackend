package it.clipd.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by toroptsev on 30.05.16.
 */
public class BaseResponse implements Serializable {

    @JsonProperty
    private ResponseCode responseCode;

    @JsonProperty
    private String responseMessage;

    public BaseResponse(ResponseCode responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public BaseResponse() {
        responseCode = ResponseCode.OK;
    }

    public ResponseCode getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
