package it.clipd.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by toroptsev on 25.07.16.
 */
public class LinkResponse extends BaseResponse {

    @JsonProperty
    private String linkCode;

    public LinkResponse() {}

    public LinkResponse(ResponseCode responseCode, String responseMessage) {
        super(responseCode, responseMessage);
    }

    public String getLinkCode() {
        return linkCode;
    }

    public void setLinkCode(String linkCode) {
        this.linkCode = linkCode;
    }
}
