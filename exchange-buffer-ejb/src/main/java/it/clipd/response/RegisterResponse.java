package it.clipd.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by toroptsev on 28.05.16.
 */
public class RegisterResponse extends BaseResponse{

    @JsonProperty
    private String token;

    @JsonProperty
    private String uuid;

    public RegisterResponse() {
        super();
    }

    public RegisterResponse(ResponseCode responseCode, String responseMessage) {
        super(responseCode, responseMessage);
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
