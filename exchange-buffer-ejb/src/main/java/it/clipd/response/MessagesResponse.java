package it.clipd.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.clipd.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toroptsev on 08.06.16.
 */
public class MessagesResponse extends BaseResponse {

    @JsonProperty
    private List<Message> messages = new ArrayList<>();

    public MessagesResponse(ResponseCode responseCode, String responseMessage) {
        super(responseCode, responseMessage);
    }

    public MessagesResponse(List<Message> messages) {
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
