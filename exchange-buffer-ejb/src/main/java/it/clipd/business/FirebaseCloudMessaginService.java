package it.clipd.business;

import com.squareup.okhttp.*;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;

/**
 * Created by toroptsev on 01.06.16.
 */
@Stateless
public class FirebaseCloudMessaginService {

    @Inject
    private Logger logger;

    private static final String URL = "https://fcm.googleapis.com/fcm/send";
    private static final String SERVER_KEY = "key=AIzaSyCu1lnPaGd-keHOk2CB0sOSHwg3FTqtnRc";
    private static final String DEBUG_SERVER_KEY = "key=AIzaSyBtgorswtfjCfyGZ1WYQWSkxLd6ArBt3D8";
    private static final String CONTENT_TYPE = "application/json";

    public void sendDebugMessage(String token, String title, String message) {
        sendMessage(token, title, message, true);
    }

    public void sendMessage(String token, String title, String message) {
        sendMessage(token, title, message, false);
    }

    private void sendMessage(String token, String title, String message, boolean debug) {
        logger.info("Sending message " + message + " to token " + token);
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse(CONTENT_TYPE);
        RequestBody body = title != null ?
                RequestBody.create(mediaType, constructPayloadContent(token, title, message, PushType.NOTIFICATION)) :
                RequestBody.create(mediaType, constructPayloadContent(token, null, message, PushType.PAYLOAD));
        Request request = new Request.Builder()
                .url(URL)
                .post(body)
                .addHeader("content-type", CONTENT_TYPE)
                .addHeader("authorization", debug ? DEBUG_SERVER_KEY : SERVER_KEY)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                System.out.println("Notification sent successfully");
            } else {
                System.out.println("Notification didn't send: " + response.message());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String constructPayloadContent(String token, String title, String message, PushType type) {
        // TODO change to MappligClass
        String content = "{\n" +
                "   \"to\" : \"" + token + "\",\n" +
                "   \"time_to_live\" : 300" + // 5 minutes expiring
//                "   \"time_to_live\" : 5" + // 5 seconds expiring
                "   \"data\" : {\n" +
                "     \"type\" : \"" + type + "\"\n" +
                (title != null ? "     \"title\" : \"" + title + "\"\n" : "") +
                "     \"body\" : \"" + message + "\"\n" +
                "   }\n" +
                " }";
        System.out.println("Message content: " + content);
        return content;
    }

    private String constructPushContent(String token, String title, String message) {
        // TODO change to MappligClass
        String content = "{\n" +
                "\"notification\": {" +
                "   \"title\": \"" + title + "\"," +
                "   \"body\": \"" + message + "\"" +
                "   }," +
                "   \"to\": \"" + token + "\"" +
                "}";
        System.out.println("Message content: " + content);
        return content;
    }

    private enum PushType {
        NOTIFICATION,
        PAYLOAD
    }
}
