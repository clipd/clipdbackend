package it.clipd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.BsonDocument;
import org.bson.BsonDocumentWrapper;
import org.bson.codecs.configuration.CodecRegistry;

/**
 * Created by toroptsev on 01.06.16.
 */
public class Session extends BaseModel {

    @JsonProperty
    private User user;
    @JsonProperty
    private String token;
    @JsonProperty
    private String notifyToken;
    @JsonProperty
    private String versionRelease;
    @JsonProperty
    private String versionIncremental;
    @JsonProperty
    private String versionSdk;
    @JsonProperty
    private String board;
    @JsonProperty
    private String brand;
    @JsonProperty
    private String device;
    @JsonProperty
    private String deviceType;
    @JsonProperty
    private String deviceId;
    @JsonProperty
    private String versionCode;
    @JsonProperty
    private String versionName;
    @JsonProperty
    private String ip;
    @JsonProperty
    private String locale;
    @JsonProperty
    private String packageName;
    @JsonProperty
    private Boolean active = true;
    @JsonProperty
    private String uuid;

    @Override
    public <TDocument> BsonDocument toBsonDocument(Class<TDocument> aClass, CodecRegistry codecRegistry) {
        return new BsonDocumentWrapper<>(this, codecRegistry.get(Session.class));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return brand + " " + device;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setNotifyToken(String notifyToken) {
        this.notifyToken = notifyToken;
    }

    public String getNotifyToken() {
        return notifyToken;
    }

    public String getVersionRelease() {
        return versionRelease;
    }

    public void setVersionRelease(String versionRelease) {
        this.versionRelease = versionRelease;
    }

    public String getVersionIncremental() {
        return versionIncremental;
    }

    public void setVersionIncremental(String versionIncremental) {
        this.versionIncremental = versionIncremental;
    }

    public String getVersionSdk() {
        return versionSdk;
    }

    public void setVersionSdk(String versionSdk) {
        this.versionSdk = versionSdk;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDevice() {
        return device;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void clearPrivateInfo() {
        super.clearPrivateInfo();
        setUser(null);
        setToken(null);
        setNotifyToken(null);
    }

    public Session getSessionForMessage() {
        Session clearSession = new Session();
        clearSession.setBrand(getBrand());
        clearSession.setDevice(getDevice());
        return clearSession;
    }
}
