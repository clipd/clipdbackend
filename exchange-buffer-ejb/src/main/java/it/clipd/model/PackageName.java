package it.clipd.model;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by toroptsev on 04.06.16.
 */
public enum PackageName {

    ANDROID_PACKAGE_NAME("it.clipd.android"),
    ANDROID_DEBUG_PACKAGE_NAME("it.clipd.android.debug");

    private String packageName;

    PackageName(String packageName) {
        this.packageName = packageName;
    }

    public static PackageName getByPackageName(String packageName) {
        Optional<PackageName> packageOptional = Arrays.stream(PackageName.values())
                .filter(pkg -> pkg.packageName.equals(packageName))
                .findFirst();
        if (packageOptional.isPresent()) {
            return packageOptional.get();
        } else {
            return null;
        }
    }

    public boolean isDebug() {
        return ANDROID_DEBUG_PACKAGE_NAME == this;
    }
}
