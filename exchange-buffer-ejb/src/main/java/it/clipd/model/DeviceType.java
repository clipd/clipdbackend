package it.clipd.model;

import java.io.Serializable;

/**
 * Created by toroptsev on 29.05.16.
 */
public enum DeviceType implements Serializable {

    IOS, ANDROID, WEB
}
