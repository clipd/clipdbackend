package it.clipd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.BsonDocument;
import org.bson.BsonDocumentWrapper;
import org.bson.codecs.configuration.CodecRegistry;

/**
 * Created by toroptsev on 06.06.16.
 */
public class Message extends BaseModel {

    @JsonProperty
    private String text;

    @JsonProperty
    private Session session;

    @JsonProperty
    private String uuid;

    @Override
    public <TDocument> BsonDocument toBsonDocument(Class<TDocument> aClass, CodecRegistry codecRegistry) {
        return new BsonDocumentWrapper<>(this, codecRegistry.get(Message.class));
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public void clearPrivateInfo() {
        super.clearPrivateInfo();
        if (getSession() != null) {
            setSession(getSession().getSessionForMessage());
        }
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
