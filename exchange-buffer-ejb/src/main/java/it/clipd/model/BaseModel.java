package it.clipd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by toroptsev on 06.06.16.
 */
public abstract class BaseModel implements Serializable, Bson {

//    @JsonProperty("_id")
    protected ObjectId id;

    @JsonProperty
    protected Long lastModified = Calendar.getInstance().getTimeInMillis();

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public void clearPrivateInfo() {
        setId(null);
    }
}
