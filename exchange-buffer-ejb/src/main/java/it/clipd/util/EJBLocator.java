package it.clipd.util;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by toroptsev on 08.06.16.
 */
public class EJBLocator {

    public static <I> I lookupLocalWild(Class<? extends I> clazz) {
        try {
            return (I) InitialContext.doLookup(getJNDILocalNameWild(clazz.getSimpleName()));
        } catch (NamingException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage(), e);
//			return null;
        }
    }

    private static String getJNDILocalNameWild(String className) throws NamingException {
//		String appName = InitialContext.doLookup("java:app/AppName");
//		String moduleName = InitialContext.doLookup("java:module/ModuleName");
//		return new StringBuilder("java:app/").append(appName).append("/").append(moduleName).append("/").append(className).toString();
        return new StringBuilder("java:app/exchange-buffer-ejb/").append(className).toString();
    }
}
