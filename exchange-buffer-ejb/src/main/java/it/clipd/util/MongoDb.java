package it.clipd.util;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import it.clipd.codec.MessageCodec;
import it.clipd.codec.SessionCodec;
import it.clipd.codec.UserCodec;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;

/**
 * Created by toroptsev on 24.12.15.
 */
public class MongoDb {
    private MongoDatabase dbInstance;
    private static volatile MongoDb instance;
    
    public static MongoDb getInstance() {
        MongoDb localInstance = instance;
        if (localInstance == null) {
            synchronized (MongoDb.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MongoDb();
                }
            }
        }
        return localInstance;
    }
    
    private MongoDb() {
        Codec<Document> defaultDocumentCodec = MongoClient.getDefaultCodecRegistry().get(Document.class);
        UserCodec userCodec = new UserCodec(defaultDocumentCodec);
        SessionCodec sessionCodec = new SessionCodec(defaultDocumentCodec);
        MessageCodec messageCodec = new MessageCodec(defaultDocumentCodec);

        CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
                MongoClient.getDefaultCodecRegistry(),
                CodecRegistries.fromCodecs(userCodec, sessionCodec, messageCodec));

        MongoClientOptions options =MongoClientOptions.builder().codecRegistry(codecRegistry).build();
        dbInstance = new MongoClient(new ServerAddress(), options).getDatabase("main");
    }

    public MongoDatabase getDbInstance() {
        return dbInstance;
    }
}
