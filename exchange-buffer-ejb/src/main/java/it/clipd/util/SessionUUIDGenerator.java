package it.clipd.util;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.UUID;

/**
 * Created by toroptsev on 28.05.16.
 */
@Stateless
public class SessionUUIDGenerator {

    @Inject
    private MongoDatabase database;

    public String generateToken() {
        String token = UUID.randomUUID().toString();

        while (database.getCollection("sessions").count(Filters.eq("token", token)) > 0) {
            token = UUID.randomUUID().toString();
        }

        return token;
    }

    public String generateUUID() {
        String uuid = UUID.randomUUID().toString().toUpperCase();

        while (database.getCollection("sessions").count(Filters.eq("uuid", uuid)) > 0) {
            uuid = UUID.randomUUID().toString().toUpperCase();
        }

        return uuid;
    }
}


