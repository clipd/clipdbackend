package it.clipd.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by toroptsev on 10.06.16.
 */
public class Test {

    public static void main(String[] args) {
        Pattern p = Pattern.compile("[\\d]{2}\\.[\\d]{2}\\.[\\d]{4}");
        Matcher m = p.matcher("31.03.1986");
        System.out.println(m.matches());
    }
}
