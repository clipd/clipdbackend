package it.clipd.util;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Random;

/**
 * Created by toroptsev on 28.05.16.
 */
@Stateless
public class LinkCodeGenerator {

    @Inject
    private MongoDatabase database;

    public String generate() {
        String linkCode = generate5DigitsCode();

        while (database.getCollection("users").count(Filters.eq("linkCode", linkCode)) > 0) {
            linkCode = generate5DigitsCode();
        }

        return linkCode;
    }

    private String generate5DigitsCode() {
        Random r = new Random();
        int i = r.nextInt(999999);
        return String.format("%05d", i);
    }
}
