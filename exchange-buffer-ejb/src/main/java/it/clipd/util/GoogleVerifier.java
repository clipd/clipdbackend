package it.clipd.util;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import it.clipd.model.PackageName;
import it.clipd.model.User;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

/**
 * Created by toroptsev on 29.05.16.
 */
@Stateless
public class GoogleVerifier {

    @Inject
    private Logger logger;

    // FIXME перенести куда-то

    private static final String ANDROID_CLIENT_ID = "585788175855-l5gsuf4c506pbuab3u3g0oa41v293mpc.apps.googleusercontent.com";
    private static final String ANDROID_DEBUG_CLIENT_ID = "31536831308-iq3nbm6hn5g71vsr51kef6u9ti61hjii.apps.googleusercontent.com";
    private static final String ANDROID_CLIENT_SECRET_KEY = "bjUPdi613BM_tb7xVgtWHYM7";
    private static final String ANDROID_DEBUG_CLIENT_SECRET_KEY = "SfANnAdAdiePFSs8v1a4tM5I";

//    The ID token is a JWT that is properly signed with an appropriate Google public key (available in JWK or PEM format).
//    The value of aud in the ID token is equal to one of your app's client IDs.
//    This check is necessary to prevent ID tokens issued to a malicious app being used to access data about the same user on your app's backend server.
//    The value of iss in the ID token is equal to accounts.google.com or https://accounts.google.com.
//    The expiry time (exp) of the ID token has not passed.
//    If your authentication request specified a hosted domain, the ID token has a hd claim that matches your Google Apps hosted domain.
//    Rather than writing your own code to perform these verification steps, we strongly recommend using a Google API client library for your platform, or calling our tokeninfo validation endpoint.

    public User verify(String idTokenString, String email, String packageName) throws GoogleVerificationException {

        String clientId;
        PackageName pkg = PackageName.getByPackageName(packageName);
        if (pkg == null) {
            throw new GoogleVerificationException("Unsupported packageName: " + packageName);
        } else {
            if (pkg.isDebug()) {
                clientId = ANDROID_DEBUG_CLIENT_ID;
            } else {
                clientId = ANDROID_CLIENT_ID;
            }
        }
        logger.info("Client ID: " + clientId);
        HttpTransport transport = null;
        try {
            transport = GoogleNetHttpTransport.newTrustedTransport();
        } catch (GeneralSecurityException | IOException e) {
            logger.error("Failed to create new Http transport");
            throw new GoogleVerificationException(e);
        }
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList(clientId)) // TODO может быть нужно сразу два передавать
                // If you retrieved the token on Android using the Play Services 8.3 API or newer, set
                // the issuer to "https://accounts.google.com". Otherwise, set the issuer to
                // "accounts.google.com". If you need to verify tokens from multiple sources, build
                // a GoogleIdTokenVerifier for each issuer and try them both.
                .setIssuer("https://accounts.google.com")
                .build();

// (Receive idTokenString by HTTPS POST)

        GoogleIdToken idToken;
        try {
            idToken = verifier.verify(idTokenString);
        } catch (GeneralSecurityException | IOException e) {

            String msg = "Failed to verify idToken: " + e.getMessage();
            logger.error(msg, e);
            throw new GoogleVerificationException(msg);
        }
        if (idToken == null) {
            String msg = "Invalid ID token";
            logger.warn(msg);
            throw new GoogleVerificationException(msg);
        }
        GoogleIdToken.Payload payload = idToken.getPayload();

        // Print user identifier
        String userId = payload.getSubject();
        logger.info("User ID: " + userId);

        // Get profile information from payload
        String emailFromPayload = payload.getEmail();
        boolean emailVerified = payload.getEmailVerified();
        logger.info("Email verified: " + (emailVerified ? "true" : "false"));
        String name = (String) payload.get("name");
        logger.info("Name: " + name);
        String pictureUrl = (String) payload.get("picture");
        logger.info("Picture: " + pictureUrl);
        String locale = (String) payload.get("locale");
        logger.info("Locale: " + locale);
        String familyName = (String) payload.get("family_name");
        logger.info("Family name: " + familyName);
        String givenName = (String) payload.get("given_name");
        logger.info("Given name: " + givenName);

        if (!emailVerified) {
            String msg = "Email is not verified";
            logger.warn(msg);
            throw new GoogleVerificationException(msg);
        }
        if (!email.equalsIgnoreCase(emailFromPayload)) {
            String msg = "Given email " + email + " not equal " + emailFromPayload;
            logger.warn(msg);
            throw new GoogleVerificationException(msg);
        }

        User user = new User();
        user.setEmail(emailFromPayload);
        user.setName(name);
        user.setPicture(pictureUrl);
        user.setLocale(locale);
        user.setFamilyName(familyName);
        user.setGivenName(givenName);

        return user;
    }

    public static class GoogleVerificationException extends Exception {

        public GoogleVerificationException() {
            super();
        }

        public GoogleVerificationException(String message) {
            super(message);
        }

        public GoogleVerificationException(Exception e) {
            super(e);
        }



    }
}
