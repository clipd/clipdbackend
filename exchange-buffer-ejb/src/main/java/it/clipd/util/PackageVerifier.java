package it.clipd.util;

import it.clipd.model.Session;

/**
 * Created by toroptsev on 04.06.16.
 */
public class PackageVerifier {

    public static final String ANDROID_PACKAGE_NAME = "it.clipd.android";
    public static final String ANDROID_DEBUG_PACKAGE_NAME = "it.clipd.android.debug";

    public static boolean isDebug(Session session) {
        return session.getPackageName() != null && session.getPackageName().equals(ANDROID_DEBUG_PACKAGE_NAME);
    }

}
