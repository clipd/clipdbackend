package it.clipd.codec;

import it.clipd.model.User;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

/**
 * Created by toroptsev on 31.05.16.
 */
public class UserCodec extends BaseCodec<User> {

    public UserCodec(Codec<Document> codec) {
        super(codec);
    }

    @Override
    public User decode(BsonReader reader, DecoderContext decoderContext) {
        Document document = documentCodec.decode(reader, decoderContext);

        User user = new User();

        user.setId(document.getObjectId("_id"));
        user.setEmail(document.getString("email"));
        user.setPicture(document.getString("picture"));
        user.setName(document.getString("name"));
        user.setFamilyName(document.getString("familyName"));
        user.setGivenName(document.getString("givenName"));
        user.setLocale(document.getString("locale"));
        user.setLastModified(document.getLong("lastModified"));
        user.setLinkCode(document.getString("linkCode"));

        return user;
    }

    @Override
    public void encode(BsonWriter writer,
                       User user,
                       EncoderContext encoderContext)
    {
        Document document = new Document();

        if (user.getId() != null) {
            document.put("_id", user.getId());
        }
        if (user.getEmail() != null) {
            document.put("email", user.getEmail());
        }
        if (user.getPicture() != null) {
            document.put("picture", user.getPicture());
        }
        if (user.getName() != null) {
            document.put("name", user.getName());
        }
        if (user.getFamilyName() != null) {
            document.put("familyName", user.getFamilyName());
        }
        if (user.getGivenName() != null) {
            document.put("givenName", user.getGivenName());
        }
        if (user.getLocale() != null) {
            document.put("locale", user.getLocale());
        }
        if (user.getLastModified() != null) {
            document.put("lastModified", user.getLastModified());
        }
        if (user.getLinkCode() != null) {
            document.put("linkCode", user.getLinkCode());
        }

        documentCodec.encode(writer, document, encoderContext);
    }
}
