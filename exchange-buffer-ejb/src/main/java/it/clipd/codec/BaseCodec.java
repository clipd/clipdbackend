package it.clipd.codec;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.clipd.model.BaseModel;
import org.bson.*;
import org.bson.codecs.*;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;

/**
 * Created by toroptsev on 06.06.16.
 */
public abstract class BaseCodec<T extends BaseModel> implements CollectibleCodec<T> {

    protected Codec<Document> documentCodec;

    BaseCodec() {
        this.documentCodec = new DocumentCodec();
    }

    BaseCodec(Codec<Document> codec) {
        this.documentCodec = codec;
    }

    @Override
    public T generateIdIfAbsentFromDocument(T model) {
        if (!documentHasId(model))
        {
            model.setId(ObjectId.get());
        }
        return model;
    }

    @Override
    public boolean documentHasId(T model) {
        return model.getId() != null;
    }

    @Override
    public BsonValue getDocumentId(T model) {
        if (!documentHasId(model))
        {
            throw new IllegalStateException("The document does not contain an _id");
        }

        return new BsonObjectId(model.getId());
    }

    @Override
    public Class<T> getEncoderClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public T decode(BsonReader reader, DecoderContext decoderContext) {
        Document document = documentCodec.decode(reader, decoderContext);

        String json = document.toJson();
        System.out.println("Json after decode: " + json);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, getEncoderClass());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

//        user.setId(document.getObjectId("_id"));
//        user.setEmail(document.getString("email"));
//        user.setPicture(document.getString("picture"));
//        user.setName(document.getString("name"));
//        user.setFamilyName(document.getString("familyName"));
//        user.setGivenName(document.getString("givenName"));
//        user.setLocale(document.getString("locale"));
//        user.setLastModified(document.getLong("lastModified"));
//
//        return model;
    }

    @Override
    public void encode(BsonWriter writer, T model, EncoderContext encoderContext)
    {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(model);
            System.out.println("Json before encode: " + json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }
        Document document = Document.parse(json);

//        if (model.getId() != null) {
//            document.put("_id", model.getId());
//        }
//        if (user.getEmail() != null) {
//            document.put("email", user.getEmail());
//        }
//        if (user.getPicture() != null) {
//            document.put("picture", user.getPicture());
//        }
//        if (user.getName() != null) {
//            document.put("name", user.getName());
//        }
//        if (user.getFamilyName() != null) {
//            document.put("familyName", user.getFamilyName());
//        }
//        if (user.getGivenName() != null) {
//            document.put("givenName", user.getGivenName());
//        }
//        document.put("locale", user.getLocale());
//        document.put("lastModified", user.getLastModified());

        documentCodec.encode(writer, document, encoderContext);
    }
}
