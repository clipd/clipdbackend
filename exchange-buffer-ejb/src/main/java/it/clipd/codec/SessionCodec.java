package it.clipd.codec;

import it.clipd.dao.UserDAO;
import it.clipd.model.Session;
import it.clipd.util.EJBLocator;
import org.apache.commons.lang.StringUtils;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.types.ObjectId;

/**
 * Created by toroptsev on 01.06.16.
 */
public class SessionCodec extends BaseCodec<Session> {

    public SessionCodec(Codec<Document> codec) {
        super(codec);
    }

    @Override
    public Session decode(BsonReader bsonReader, DecoderContext decoderContext) {
        Document document = documentCodec.decode(bsonReader, decoderContext);

        UserDAO userDAO = EJBLocator.lookupLocalWild(UserDAO.class);

        Session session = new Session();

        session.setId(document.getObjectId("_id"));

        ObjectId userId = document.getObjectId("userId");
        session.setUser(userDAO.findById(userId));

        // TODO WTF???
        session.setToken(document.getString("token"));
        session.setNotifyToken(document.getString("notifyToken"));
        session.setVersionRelease(document.getString("versionRelease"));
        session.setVersionIncremental(document.getString("versionIncremental"));
        session.setVersionSdk(document.getString("versionSdk"));
        session.setBoard(document.getString("board"));
        session.setBrand(document.getString("brand"));
        session.setDevice(document.getString("device"));
        session.setDeviceType(document.getString("deviceType"));
        session.setDeviceId(document.getString("deviceId"));
        session.setVersionCode(document.getString("versionCode"));
        session.setVersionName(document.getString("versionName"));
        session.setIp(document.getString("ip"));
        session.setLocale(document.getString("locale"));
        session.setPackageName(document.getString("packageName"));
        session.setLastModified(document.getLong("lastModified"));
        session.setActive(document.getBoolean("active"));
        session.setUuid(document.getString("uuid"));

        return session;
    }

    @Override
    public void encode(BsonWriter bsonWriter, Session session, EncoderContext encoderContext) {
        Document document = new Document();

        // TODO WTF???
        if (session.getId() != null) {
            document.put("_id", session.getId());
        }
        if (session.getUser() != null) {
            document.put("userId", session.getUser().getId());
        }
        if (session.getToken() != null) {
            document.put("token", session.getToken());
        }
        if (StringUtils.isNotEmpty(session.getNotifyToken())) {
            document.put("notifyToken", session.getNotifyToken());
        }
        if (StringUtils.isNotEmpty(session.getVersionRelease())) {
            document.put("versionRelease", session.getVersionRelease());
        }
        if (StringUtils.isNotEmpty(session.getVersionIncremental())) {
            document.put("versionIncremental", session.getVersionIncremental());
        }
        if (StringUtils.isNotEmpty(session.getVersionSdk())) {
            document.put("versionSdk", session.getVersionSdk());
        }
        if (StringUtils.isNotEmpty(session.getBoard())) {
            document.put("board", session.getBoard());
        }
        if (StringUtils.isNotEmpty(session.getBrand())) {
            document.put("brand", session.getBrand());
        }
        if (StringUtils.isNotEmpty(session.getDevice())) {
            document.put("device", session.getDevice());
        }
        if (session.getDeviceType() != null) {
            document.put("deviceType", session.getDeviceType());
        }
        if (StringUtils.isNotEmpty(session.getDeviceId())) {
            document.put("deviceId", session.getDeviceId());
        }
        if (StringUtils.isNotEmpty(session.getVersionCode())) {
            document.put("versionCode", session.getVersionCode());
        }
        if (StringUtils.isNotEmpty(session.getVersionName())) {
            document.put("versionName", session.getVersionName());
        }
        if (StringUtils.isNotEmpty(session.getIp())) {
            document.put("ip", session.getIp());
        }
        if (StringUtils.isNotEmpty(session.getLocale())) {
            document.put("locale", session.getLocale());
        }
        if (StringUtils.isNotEmpty(session.getPackageName())) {
            document.put("packageName", session.getPackageName());
        }
        if (session.getLastModified() != null) {
            document.put("lastModified", session.getLastModified());
        }
        if (session.getActive() != null) {
            document.put("active", session.getActive());
        }
        if (session.getUuid() != null) {
            document.put("uuid", session.getUuid());
        }

        documentCodec.encode(bsonWriter, document, encoderContext);
    }

}
