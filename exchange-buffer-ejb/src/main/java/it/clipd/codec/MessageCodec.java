package it.clipd.codec;

import it.clipd.dao.SessionDAO;
import it.clipd.model.Message;
import it.clipd.util.EJBLocator;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.types.ObjectId;

/**
 * Created by toroptsev on 06.06.16.
 */
public class MessageCodec extends BaseCodec<Message> {

    public MessageCodec(Codec<Document> defaultDocumentCodec) {
        super(defaultDocumentCodec);
    }

    @Override
    public Message decode(BsonReader reader, DecoderContext decoderContext) {
        Document document = documentCodec.decode(reader, decoderContext);

        Message message = new Message();

        SessionDAO sessionDAO = EJBLocator.lookupLocalWild(SessionDAO.class);

        message.setId(document.getObjectId("_id"));

        ObjectId sessionId = document.getObjectId("sessionId");
        message.setSession(sessionDAO.findById(sessionId));

        message.setText(document.getString("text"));
        message.setLastModified(document.getLong("lastModified"));
        message.setUuid(document.getString("uuid"));

        return message;
    }

    @Override
    public void encode(BsonWriter writer,
                       Message message,
                       EncoderContext encoderContext)
    {
        Document document = new Document();

        if (message.getId() != null) {
            document.put("_id", message.getId());
        }
        if (message.getSession() != null) {
            document.put("userId", message.getSession().getUser().getId());
        }
        if (message.getSession() != null) {
            document.put("sessionId", message.getSession().getId());
        }
        if (message.getText() != null) {
            document.put("text", message.getText());
        }
        if (message.getLastModified() != null) {
            document.put("lastModified", message.getLastModified());
        }
        if (message.getUuid() != null) {
            document.put("uuid", message.getUuid());
        }

        documentCodec.encode(writer, document, encoderContext);
    }
}
