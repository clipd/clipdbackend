package it.clipd.dao;

import com.mongodb.client.result.UpdateResult;
import it.clipd.model.Session;
import it.clipd.model.User;
import org.bson.Document;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by toroptsev on 01.06.16.
 */
@Stateless
public class SessionDAO extends BaseDAO<Session> {

    @Override
    protected String getCollectionName() {
        return "sessions";
    }

    public Session findByToken(@NotNull String token) {
//        return getCollection().find(Filters.eq("token", token), Session.class).first();
        // TODO учитывать expiration токена

        return getCollection()
                .find(new Document("token", token)
                        .append("active", true))
                .first();
//        return getCollection().find(Filters.eq("token", token)).first();
    }

    public Session findByLinkCode(@NotNull String linkCode) {
        // TODO учитывать expiration кода
//        return getCollection().find(Filters.eq("linkCode", linkCode)).first();
        return getCollection()
                .find(new Document("linkCode", linkCode)
                        .append("active", true))
                .first();
    }

    public List<Session> listByUser(@NotNull User user) {
//        return getCollection().find(Filters.eq("userId", user.getId()), Session.class).into(new ArrayList<>());
        return getCollection()
                .find(new Document("userId", user.getId())
                        .append("active", true))
                .sort(new Document("lastModified", 1))
                .into(new ArrayList<>());
    }

    public List<Session> listWithNotifyTokenByUser(@NotNull User user) {
//        return getCollection().find(Filters.eq("userId", user.getId()), Session.class).into(new ArrayList<>());
        return getCollection()
                .find(new Document("userId", user.getId())
                        .append("notifyToken", new Document("$exists", true))
                        .append("active", true))
                .into(new ArrayList<>());
    }

    public boolean hasAnySession(@NotNull User user) {
//        return getCollection().count(Filters.eq("userId", user.getId())) > 0;
        return getCollection().count(new Document("userId", user.getId()).append("active", true)) > 0;
    }

    public long deleteByUser(@NotNull User user) {
        UpdateResult result = getCollection()
                .updateMany(new Document("userId", user.getId()).append("active", true), 
                        new Document("$set",
                                new Document("active", false)
                                        .append("lastModified", Calendar.getInstance().getTimeInMillis())));
        return result.getModifiedCount();
    }

    public boolean assignNotifyToken(@NotNull String token, @NotNull String notifyToken) {
        UpdateResult result = getCollection()
                .updateOne(new Document("token", token).append("active", true), 
                        new Document("$set",
                                new Document("notifyToken", notifyToken)
                                        .append("lastModified", Calendar.getInstance().getTimeInMillis())));
        return result.getModifiedCount() == 1;
    }

    public boolean deleteByUuid(@NotNull String uuid) {
        UpdateResult result = getCollection()
                .updateOne(new Document("uuid", uuid),
                        new Document("$set", new Document("active", false)
                                .append("lastModified", Calendar.getInstance().getTimeInMillis()))
                            .append("$unset", new Document("notifyToken", null)));
        return result.getModifiedCount() == 1;
    }
}
