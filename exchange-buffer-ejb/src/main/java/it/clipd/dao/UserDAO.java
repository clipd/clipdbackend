package it.clipd.dao;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import it.clipd.model.Session;
import it.clipd.model.User;
import org.bson.Document;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

/**
 * Created by toroptsev on 30.05.16.
 */
@Stateless
public class UserDAO extends BaseDAO<User> {
    
    @Inject
    private MongoDatabase database;

    @EJB
    private SessionDAO sessionDAO;
    
    @Override
    protected String getCollectionName() {
        return "users";
    }
    
    public long deleteByEmail(@NotNull String email) {
        User user = findByEmail(email);
        if (user == null) {
            return 0;
        }

        long deleteSessionCount = sessionDAO.deleteByUser(user);

        getCollection().deleteMany(Filters.eq("email", email));

        return deleteSessionCount;
    }

    public User findByEmail(@NotNull String email) {
//        return getCollection().find(Filters.eq("email", email), User.class).first();
        return getCollection().find(Filters.eq("email", email)).first();
    }

    public User findByToken(@NotNull String token) {
        Session session = sessionDAO.findByToken(token);
        if (session == null) {
            return null;
        }

//        return getCollection().find(Filters.eq("_id", session.getUser().getId()), User.class).first();
        return getCollection().find(Filters.eq("_id", session.getUser().getId())).first();
    }

    public User findByLinkCode(String linkCode) {
        return getCollection().find(Filters.eq("linkCode", linkCode)).first();
    }

    public boolean deleteLinkCode(User user) {
        UpdateResult result = getCollection()
                .updateOne(Filters.eq("id_", user.getId()), new Document("$unset", new Document("linkCode", null)));
        return result.getMatchedCount() == 1;
    }

//    public User update(User user) {
//        return getCollection().findOneAndUpdate(Filters.eq("_id", user.getId()), user);
//    }
}
