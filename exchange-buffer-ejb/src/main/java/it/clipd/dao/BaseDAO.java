package it.clipd.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import it.clipd.model.BaseModel;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by toroptsev on 21.10.15.
 */
public abstract class BaseDAO<T extends BaseModel> {

    @Inject
    protected MongoDatabase database;

    protected abstract String getCollectionName();

    protected MongoCollection<T> getCollection() {
        return database.getCollection(getCollectionName(), getGenericClass());
    }

    protected Class<T> getGenericClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public T findById(ObjectId id) {
        return getCollection().find(Filters.eq("_id", id)).first();
    }

    public T insertOne(T model) {
        getCollection().insertOne(model);
        return model;
    }

    public T updateOne(T model) {
        return getCollection().findOneAndReplace(Filters.eq("_id", model.getId()), model);
    }

    public void updateMany(List<T> modelList) {
        modelList.stream().forEach(model -> getCollection().findOneAndReplace(Filters.eq("_id", model.getId()), model));
    }
}
