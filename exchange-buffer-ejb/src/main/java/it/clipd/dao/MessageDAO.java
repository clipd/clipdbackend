package it.clipd.dao;

import it.clipd.model.Message;
import it.clipd.model.User;
import org.bson.Document;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by toroptsev on 06.06.16.
 */
@Stateless
@LocalBean
public class MessageDAO extends BaseDAO<Message> {

    @Override
    protected String getCollectionName() {
        return "messages";
    }

    public List<Message> listByUser(User user) {
        List<Message> result = new ArrayList<>();
        return getCollection()
                .find(new Document("userId", user.getId()))
                .sort(new Document("lastModified", -1))
                .limit(30)
                .into(result);
    }
}
